﻿using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int NORTHWEST = 0;
        private const int NORTH = 1;
        private const int NORTHEAST = 2;
        private const int WEST = 3;
        private const int CENTER = 4;
        private const int EAST = 5;
        private const int SOUTHWEST = 6;
        private const int SOUTH = 7;
        private const int SOUTHEAST = 8;

        private Cube myFirstCube = new Cube();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 2";
            OpenGL gl = args.OpenGL;

            //clear screena and depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            myFirstCube.Render(gl);

            int outcome = RandomNumberGenerator.GenerateInt(0,8);
            /*switch (outcome)
            {
                case NORTHWEST:
                    myFirstCube.posY++;
                    myFirstCube.posX--;
                    break;
                case NORTH:
                    myFirstCube.posY++;
                    break;
                case NORTHEAST:
                    myFirstCube.posY++;
                    myFirstCube.posX++;
                    break;
                case WEST:
                    myFirstCube.posX--;
                    break;
                case CENTER:
                    break;
                case EAST:
                    myFirstCube.posX++;
                    break;
                case SOUTHWEST:
                    myFirstCube.posY--;
                    myFirstCube.posX--;
                    break;
                case SOUTH:
                    myFirstCube.posY--;
                    break;
                case SOUTHEAST:
                    myFirstCube.posY--;
                    myFirstCube.posX++;
                    break;

            }*/
            myFirstCube.color = new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1));

            double outcomeWithDiffProbability = RandomNumberGenerator.GenerateDouble(0, 1);

            if (outcomeWithDiffProbability < 0.4)
            {
                myFirstCube.posX++;
            } else if (outcomeWithDiffProbability < 0.6)
            {
                myFirstCube.posY--;
            }
            else if (outcomeWithDiffProbability < 0.8)
            {
                myFirstCube.posX--;
            }
            else
            {
                myFirstCube.posY++;
            }

            //20% up
            //20% down
            //20% left
            //40% right

            gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, outcome + ""); 
        }

        #region INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}

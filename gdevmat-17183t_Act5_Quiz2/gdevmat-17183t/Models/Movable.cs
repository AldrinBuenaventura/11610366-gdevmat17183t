﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public abstract class Movable
    {
        public Vector3 Position = new Vector3();
        public Vector3 Velocity = new Vector3();
        public Vector3 Acceleration = new Vector3();
        public float Mass = 1;

        public double red = 1, green = 1, blue = 1, alpha = 1;

        public void ApplyForce(Vector3 Force)
        {
            this.Acceleration += (Force / Mass);
        }
        public abstract void Render(OpenGL gl);
    }
}

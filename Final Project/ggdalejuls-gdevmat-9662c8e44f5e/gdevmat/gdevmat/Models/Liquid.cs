﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat.Models
{
    class Liquid
    {
        public float x, y;
        public float width, depth;
        public float drag;

        public Liquid(float x, float y, float width, float depth, float drag)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.depth = depth;
            this.drag = drag;
        }

        public void Render(OpenGL gl, byte r = 28, byte g = 120, byte b = 186)
        {
            gl.Color(r, g, b);
            gl.Begin(OpenGL.GL_POLYGON);
            gl.Vertex(x - width, y, 0);
            gl.Vertex(x + width, y, 0);
            gl.Vertex(x + width, y - depth, 0);
            gl.Vertex(x - width, y - depth, 0);
            gl.End();
        }

        public bool Contains(Movable movable)
        {
            var p = movable.Position;
            return p.x > this.x - this.width && p.x < this.x + this.width && p.y < this.y;
        }

        public Vector3 CalculateDragForce(Movable movable)
        {
            var speed = movable.Velocity.GetLength();
            var dragMagnitude = this.drag * speed * speed;

            var dragForce = movable.Velocity;
            dragForce *= -1;
            dragForce.Normalize();
            dragForce *= dragMagnitude;

            return dragForce;
        }
    }
}

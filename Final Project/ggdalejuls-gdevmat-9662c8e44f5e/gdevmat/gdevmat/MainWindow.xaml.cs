﻿using gdevmat.Models;
using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Initialization
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion

        private Vector3 mousePosition = new Vector3();

        private Cube myCube = new Cube()
        {
            Scale = new Vector3(0.5f, 0.5f, 0f),
            Position = new Vector3(27f, -20f, 0f),
            Mass = 5
        };

        private List<Cube> player = new List<Cube>();

        private List<Circle> circles = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();


        public double distance;
        int i = 0;
        int j = 0;

        float FrameCount = 0;

        public Vector3 direction = new Vector3();

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            // Clear The Screen And The Depth Buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            // Move Left And Into The Screen
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -50.0f);

            direction.x = mousePosition.x;
            direction.y = mousePosition.y;
            direction.Normalize();
            FrameCount++;

            while (i < 1)
            {
                player.Add(new Cube()
                {
                    Scale = new Vector3(0.5f, 0.5f, 0f),
                    Position = new Vector3(0f, 0f, 0f),
                    //(27, -20, 0)
                    Mass = 5
                });
                i++;
            }

            foreach (var x in player)
            {
                x.ApplyForce(direction / 10);
                x.Render(gl);
                if (x.Position.x < -27 || x.Position.x > 27 || x.Position.y > 20 || x.Position.y < -20)
                {
                    x.Position.x = 0f;
                    x.Position.y = 0f;
                    FrameCount = 0;
                    x.Acceleration = new Vector3(0,0,0);
                    x.Velocity = new Vector3(0, 0, 0);
                    circles.Clear();
                }
                foreach (var xd in circles)
                {
                    //distance = Math.Sqrt(x.Position.x);
                    distance = Math.Sqrt((x.Position.x - xd.Position.x) * (x.Position.x - xd.Position.x)
                        + ((x.Position.y - xd.Position.y) * (x.Position.y - xd.Position.y)));
                    if (distance <= xd.radius)
                    {
                        x.Position.x = 0f;
                        x.Position.y = 0f;
                        FrameCount = 0;
                        x.Acceleration = new Vector3(0, 0, 0);
                        x.Velocity = new Vector3(0, 0, 0);
                        circles.Clear();
                        break;
                    }
                }
                
            }

            if (FrameCount % 20 == 0)
            {
                circles.Add(new Circle()
                {
                    color = new Models.Color(1,0,0, 1),
                    Position = new Vector3(RandomNumberGenerator.GenerateFloat(0, 27), RandomNumberGenerator.GenerateFloat(0, 20), 0),
                    radius = RandomNumberGenerator.GenerateInt(1, 2),
                    Mass = RandomNumberGenerator.GenerateInt(1, 5)
                });
                j++;
            }

            


            foreach (var c in circles)
            {
                c.Render(gl);
                foreach (var h in player)
                {
                    
                    c.ApplyForce(h.CalculateAttraction(c));
                }

            }

            gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, FrameCount + " ");



        }

        #region Mouse Func
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
        }
        #endregion
    }
}

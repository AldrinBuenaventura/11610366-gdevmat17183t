﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t.Models
{
    public class Cube : Movable
    {
        public Color color = new Color();

        public Cube(float x = 0, float y = 0)
        {

        }

        public override void Render(OpenGL gl)
        {
            gl.Color(color.red, color.green, color.blue);
            float outcome = RandomNumberGenerator.GenerateInt(0, 1);
            float outcome1 = RandomNumberGenerator.GenerateInt(0, 1);
            float outcome2 = RandomNumberGenerator.GenerateInt(0, 1);
            gl.Begin(OpenGL.GL_QUADS);
            gl.Color(outcome, outcome1, outcome2);
            gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
            gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);


            gl.End();
        }
    }
}

﻿using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Cube myCube = new Cube()
        {
            posX = 5,
            posY = 0
        };

        private Circle myCircle = new Circle()
        {
            posX = 5,
            posY = 0
        };

        private List<Circle> circles = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();

        public MainWindow()
        {
            InitializeComponent();
        }

        int x = 0;

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 4";
            OpenGL gl = args.OpenGL;

            //clear screena and depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            circles.Add(new Circle()
            {
                red = 0.6d,
                green = 0.3d,
                blue = 0.6d,
                alpha = 0.1d,
                posX = (float)RandomNumberGenerator.GenerateGaussian(0, 15),
                posY = (float)RandomNumberGenerator.GenerateGaussian(0, 15),
                radius = RandomNumberGenerator.GenerateInt(0, 25),
            });

            cubes.Add(new Cube()
            {
                posX = (float)RandomNumberGenerator.GenerateGaussian(0, 15),
                posY = (float)RandomNumberGenerator.GenerateGaussian(0, 15)
            });


            foreach (var c in circles)
            {
                c.Render(gl);

                    
            }

            x++;
            if (x >= 50)
            {
                x = 0;
                cubes.Clear();
                circles.Clear();
            }

            foreach (var c in cubes)
            {
                c.Render(gl);
                
            }
            
        }

        #region INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
